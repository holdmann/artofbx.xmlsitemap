<?php
namespace Artofbx\Xmlsitemap;

use DOMDocument;
use DOMProcessingInstruction;

class Wrapper
{

    public static function apply($fileName)
    {
        $doc = new DOMDocument();

        $doc->load($_SERVER['DOCUMENT_ROOT'].$fileName);
        $doc->formatOutput = true;

        $styleheetParams = 'type="text/xsl" href="/bitrix/js/artofbx.xmlsitemap/sitemap.xsl"';

        $xmlstylesheet = new DOMProcessingInstruction(
            'xml-stylesheet',
            $styleheetParams
        );

        $doc->insertBefore($xmlstylesheet,$doc->firstChild);


        return $doc->saveXML();
    }

}
