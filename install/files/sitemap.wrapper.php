<?php
use Artofbx\Xmlsitemap\Wrapper;

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
if (CModule::IncludeModule('artofbx.xmlsitemap')) {
    header("Content-Type: text/xml");
    echo Wrapper::apply($APPLICATION->GetCurPage());
}