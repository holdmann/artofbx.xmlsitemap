<?php
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);


class artofbx_xmlsitemap extends CModule
{
    var $MODULE_ID = 'artofbx.xmlsitemap';
    var $PARTNER_NAME = 'Art of Bitrix, Россия';
    var $PARTNER_URI = 'http://bxart.ru';

    /**
     * Конструктор модуля
     */
    function __construct()
    {
        $arModuleVersion = array();
        include("version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = Loc::getMessage('MODULE_XMLSITEMAP_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('MODULE_XMLSITEMAP_DESC');
        $this->PARTNER_NAME = Loc::getMessage('MODULE_XMLSITEMAP_VENDOR_NAME');
        $this->PARTNER_URI = Loc::getMessage('MODULE_XMLSITEMAP_VENDOR_URL');
    }

    /**
     * Установка модуля
     */
    function DoInstall()
    {

        CopyDirFiles(__DIR__.'/files', $_SERVER['DOCUMENT_ROOT'].'/bitrix/tools', true, true);
        CopyDirFiles(__DIR__.'/js', $_SERVER['DOCUMENT_ROOT'].'/bitrix/js', true, true);
        self::addRules();
        RegisterModule($this->MODULE_ID);
    }

    /**
     * Метод добавляет правила обработки файлов карты сайта в файл .htaccess
     */
    private static function addRules()
    {
        self::removeRules();
        if (file_exists(self::getFileName()) && is_writable(self::getFileName()) && is_readable(self::getFileName())) {
            $content = file_get_contents(self::getFileName());
            $content = str_replace('RewriteEngine On', implode(PHP_EOL, self::getRules()), $content);
            file_put_contents(self::getFileName(), $content);
        }
    }

    private static function removeRules()
    {
        if (file_exists(self::getFileName()) && is_writable(self::getFileName()) && is_readable(self::getFileName())) {
            $rules = self::getRules();
            array_shift($rules);
            $content = file_get_contents(self::getFileName());
            foreach ($rules as $rule) {
                $content = str_replace($rule.PHP_EOL, '', $content);
                $content = str_replace($rule, '', $content);
            }
            file_put_contents(self::getFileName(), $content);
        }
    }


    private static function getFileName()
    {
        return __DIR__.'/../../../../.htaccess';
    }

    private static function getRules()
    {
        return array(
            'RewriteEngine On',
            'RewriteCond %{REQUEST_FILENAME} /sitemap.*?\.xml',
            'RewriteRule ^(.*)$ /bitrix/tools/sitemap.wrapper.php [L]',
        );
    }

    function DoUninstall()
    {
        self::removeRules();
        DeleteDirFiles(__DIR__.'/files', $_SERVER['DOCUMENT_ROOT'].'/bitrix/tools');
        DeleteDirFiles(__DIR__.'/js', $_SERVER['DOCUMENT_ROOT'].'/bitrix/js');
        UnRegisterModule($this->MODULE_ID);
    }
}