<?php
/**
 * @author Kirshin Alexandr <kirshin.as@gmail.com>
 */
$arModuleVersion = array(
    "VERSION"      => "1.0.0",
    "VERSION_DATE" => "2015-09-11 00:00:00",
);